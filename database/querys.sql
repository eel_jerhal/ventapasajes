drop database buses;
create database buses;
use buses;

CREATE TABLE `tipo_bus` (
  `tipo_bus` int(1) NOT NULL AUTO_INCREMENT,
  `cant_asientos` int(2) NOT NULL,
  `tipo_asientos` varchar(20) NOT NULL,
  `detalle_tipo` varchar(80) NOT NULL,
  PRIMARY KEY (`tipo_bus`)
);

CREATE TABLE `bus` (
  `num_bus` int(6) NOT NULL AUTO_INCREMENT,
  `tipo_bus` int(1) NOT NULL,
  `patente` varchar(6) NOT NULL,
  PRIMARY KEY (`num_bus`),
  KEY `tipo_bus` (`tipo_bus`),
  CONSTRAINT `bus_ibfk_1` FOREIGN KEY (`tipo_bus`) REFERENCES `tipo_bus` (`tipo_bus`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `comuna` (
  `id_comuna` int(3) NOT NULL AUTO_INCREMENT,
  `nombre_comuna` varchar(50) NOT NULL,
  PRIMARY KEY (`id_comuna`)
);

CREATE TABLE `terminal` (
  `id_terminal` int(3) NOT NULL AUTO_INCREMENT,
  `id_comuna` int(3) NOT NULL,
  `nombre_terminal` varchar(50) NOT NULL,
  PRIMARY KEY (`id_terminal`),
  KEY `id_comuna` (`id_comuna`),
  CONSTRAINT `terminal_ibfk_1` FOREIGN KEY (`id_comuna`) REFERENCES `comuna` (`id_comuna`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `ruta` (
  `id_ruta` int(6) NOT NULL,
  `id_terminal` int(3) NOT NULL,
  `orden` int(1) NOT NULL,
  PRIMARY KEY (`id_ruta`,`id_terminal`),
  KEY `id_terminal` (`id_terminal`),
  CONSTRAINT `ruta_ibfk_1` FOREIGN KEY (`id_terminal`) REFERENCES `terminal` (`id_terminal`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `pasaje` (
  `id_pasaje` varchar(10) NOT NULL,
  `num_bus` int(6) NOT NULL,
  `id_ruta` int(6) NOT NULL,
  `precio` int(5) NOT NULL,
  `fecha_hora_salida` DATETIME NOT NULL,
  `fecha_hora_llegada` DATETIME NOT NULL,
  PRIMARY KEY (`id_pasaje`),
  KEY `num_bus` (`num_bus`),
  KEY `id_ruta` (`id_ruta`),
  CONSTRAINT `pasaje_ibfk_1` FOREIGN KEY (`num_bus`) REFERENCES `bus` (`num_bus`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pasaje_ibfk_2` FOREIGN KEY (`id_ruta`) REFERENCES `ruta` (`id_ruta`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `usuario` (
  `rut_usuario` int(9) NOT NULL,
  `rol_usuario` int(1) DEFAULT 0,
  `nombre` varchar(50) NOT NULL,
  `apellido_mat` varchar(50) NOT NULL,
  `apellido_pat` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255),
  PRIMARY KEY (`rut_usuario`)
);

CREATE TABLE `convenio` (
  `id_convenio` int(2) NOT NULL AUTO_INCREMENT,
  `convenio` varchar(50) NOT NULL,
  `desc_convenio` float NOT NULL,
  PRIMARY KEY (`id_convenio`)
);

CREATE TABLE `convenio_usuario` (
  `rut_usuario` int(9) NOT NULL,
  `id_convenio` int(2) NOT NULL,
  PRIMARY KEY (`rut_usuario`,`id_convenio`),
  KEY `id_convenio` (`id_convenio`),
  CONSTRAINT `convenio_usuario_ibfk_1` FOREIGN KEY (`rut_usuario`) REFERENCES `usuario` (`rut_usuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `convenio_usuario_ibfk_2` FOREIGN KEY (`id_convenio`) REFERENCES `convenio` (`id_convenio`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `telefono` (
  `rut_usuario` int(9) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  PRIMARY KEY (`rut_usuario`,`telefono`),
  CONSTRAINT `telefono_ibfk_1` FOREIGN KEY (`rut_usuario`) REFERENCES `usuario` (`rut_usuario`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `boleto` (
  `id_boleto` varchar(10) NOT NULL,
  `id_pasaje` varchar(10) NOT NULL,
  `rut_usuario` int(9) NOT NULL,
  `num_asiento` int(2) NOT NULL,
  `fecha_hora_compra` DATETIME NOT NULL,
  `precio_total` int(5) NOT NULL,
  PRIMARY KEY (`id_boleto`),
  KEY `id_pasaje` (`id_pasaje`),
  KEY `rut_usuario` (`rut_usuario`),
  CONSTRAINT `boleto_ibfk_1` FOREIGN KEY (`id_pasaje`) REFERENCES `pasaje` (`id_pasaje`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `boleto_ibfk_2` FOREIGN KEY (`rut_usuario`) REFERENCES `usuario` (`rut_usuario`) ON DELETE CASCADE ON UPDATE CASCADE
);

insert into comuna values(1, "Iquique");
insert into comuna values(2, "Antofagasta");
insert into comuna values(3, "La Serena");
insert into comuna values(4, "Valparaíso");
insert into comuna values(5, "Santiago");

insert into terminal (id_comuna, nombre_terminal) values(1, "Tnal. Esmeralda");
insert into terminal (id_comuna, nombre_terminal) values(2, "Rodov. Antofagasta");
insert into terminal (id_comuna, nombre_terminal) values(3, "Serena Of.E3 TB");
insert into terminal (id_comuna, nombre_terminal) values(4, "Rodov. Valparaiso");
insert into terminal (id_comuna, nombre_terminal) values(5, "T. Borja Andes.30-41");
insert into terminal (id_comuna, nombre_terminal) values(5, "Tnal. Alameda");

insert into ruta values(1, 1, 1);
insert into ruta values(1, 3, 2);
insert into ruta values(1, 4, 3);
insert into ruta values(2, 4, 1);
insert into ruta values(2, 5, 3);
insert into ruta values(3, 6, 1);
insert into ruta values(3, 4, 3);
insert into ruta values(4, 6, 1);
insert into ruta values(4, 2, 2);
insert into ruta values(4, 1, 3);
insert into ruta values(5, 3, 1);
insert into ruta values(5, 2, 3);
insert into ruta values(6, 5, 1);
insert into ruta values(6, 4, 3);
insert into ruta values(7, 4, 1);
insert into ruta values(7, 6, 3);

insert into tipo_bus (cant_asientos, tipo_asientos, detalle_tipo) values(32, "Clásico", "Bus de un piso y un Baño");
insert into tipo_bus (cant_asientos, tipo_asientos, detalle_tipo) values(63, "Semi-Cama", "Bus de dos pisos y un baño en el segundo piso");

insert into bus (patente, tipo_bus) values("ASDF12", 1);
insert into bus (patente, tipo_bus) values("ASDF22", 1);
insert into bus (patente, tipo_bus) values("ASDF33", 2);
insert into bus (patente, tipo_bus) values("ASDF44", 2);

insert into pasaje values("11111aaaaa", 2, 1, 5000, '2019-09-21 10:00:00', '2019-09-21 14:00:00');
insert into pasaje values("22222bbbbb",	2, 2, 3000,	'2019-09-21 16:00:00', '2019-09-21 18:00:00');
insert into pasaje values("33333ccccc",	1, 5, 7000,	'2019-09-21 10:00:00', '2019-09-21 18:00:00');
insert into pasaje values("44444ddddd",	3, 3, 5000, '2019-09-21 12:00:00', '2019-09-21 16:00:00');
insert into pasaje values("55555eeeee",	4, 2, 5000, '2019-09-21 10:30:00', '2019-09-21 16:30:00');
insert into pasaje values("66666fffff",	1, 7, 5000, '2019-09-21 10:30:00', '2019-09-21 16:30:00');
insert into pasaje values("77777ggggg",	3, 7, 5000, '2019-09-21 10:35:00', '2019-09-21 16:35:00');

insert into usuario values(186431626, 0, "Jeremy", "Castro", "Espinoza", "jeremy.castroe@alumnos.uv.cl", "asdasdasdasd");
insert into usuario (rut_usuario, rol_usuario, nombre, apellido_pat, apellido_mat, email) values(123456789, 0, "Ignacio", "Tapia", "Jerez", "ignacio.tapiaj@alumnos.uv.cl");

insert into boleto values(2, "66666fffff", 123456789, 21, NOW(), 5000);
insert into boleto values(1, "66666fffff", 186431626, 20, NOW(), 5000);

SELECT DISTINCT c.id_comuna, c.nombre_comuna FROM ruta r, terminal t, comuna c WHERE r.orden = 1 AND r.id_terminal = t.id_terminal and t.id_comuna = c.id_comuna GROUP BY r.id_terminal ORDER BY c.nombre_comuna;
SELECT DISTINCT c.id_comuna, c.nombre_comuna FROM ruta r, terminal t, comuna c WHERE r.orden != 1 AND r.id_terminal = t.id_terminal and t.id_comuna = c.id_comuna GROUP BY r.id_terminal ORDER BY c.nombre_comuna;

/*SOLO HACE EL JOIN ENTRE TABLA RUTA PARA SACAR EL ORIGEN Y DESTINO EN LA MISMA TABLA*/
SELECT r1.id_ruta, r1.id_comuna as c_id_origen, r1.nombre_comuna as c_origen, r1.id_terminal as t_id_origen, r1.nombre_terminal as t_origen, r2.id_comuna as c_id_destino, r2.nombre_comuna as c_destino, r2.id_terminal as t_id_destino, r2.nombre_terminal as t_destino, r2.orden FROM 
    (select r.id_ruta, r.orden, t.id_terminal, t.nombre_terminal, c.id_comuna, c.nombre_comuna from ruta r, terminal t, comuna c where r.id_terminal = t.id_terminal and t.id_comuna = c.id_comuna) r1 LEFT OUTER JOIN
    (select r.id_ruta, r.orden, t.id_terminal, t.nombre_terminal, c.id_comuna, c.nombre_comuna from ruta r, terminal t, comuna c where r.id_terminal = t.id_terminal and t.id_comuna = c.id_comuna) r2 ON r1.id_ruta = r2.id_ruta AND r1.id_terminal != r2.id_terminal 
        WHERE r1.orden = 1 AND r2.orden != 1 order by r1.id_ruta;

/*ENTREGA EL LISTADO DE PASAJES DE X COMUNA ORIGEN Y X COMUNA DESTINO MOSTRANDO EL ORIGEN Y DESTINO EN UNA FILA*/
SELECT p.id_pasaje, tb.cant_asientos, tb.tipo_asientos , p.precio, p.fecha_hora_salida, t1.t_origen, p.fecha_hora_llegada, t1.t_destino 
  FROM pasaje p, bus b, tipo_bus tb,
  (SELECT r1.id_ruta, r1.id_comuna AS c_id_origen, r1.nombre_comuna AS c_origen, r1.id_terminal AS t_id_origen, r1.nombre_terminal AS t_origen, 
    r2.id_comuna AS c_id_destino, r2.nombre_comuna AS c_destino, r2.id_terminal AS t_id_destino, r2.nombre_terminal AS t_destino, r2.orden 
    FROM (SELECT r.id_ruta, r.orden, t.id_terminal, t.nombre_terminal, c.id_comuna, c.nombre_comuna FROM ruta r, terminal t, comuna c 
    WHERE r.id_terminal = t.id_terminal AND t.id_comuna = c.id_comuna) r1 LEFT OUTER JOIN
    (SELECT r.id_ruta, r.orden, t.id_terminal, t.nombre_terminal, c.id_comuna, c.nombre_comuna FROM ruta r, terminal t, comuna c WHERE r.id_terminal = t.id_terminal AND t.id_comuna = c.id_comuna) r2 ON r1.id_ruta = r2.id_ruta AND r1.id_terminal != r2.id_terminal 
    WHERE r1.orden = 1 AND r2.orden != 1 ORDER BY r1.id_ruta) t1
  WHERE p.num_bus = b.num_bus AND b.tipo_bus = tb.tipo_bus AND t1.id_ruta = p.id_ruta AND t1.c_id_origen = 4 AND t1.c_id_destino = 5 AND p.precio <= 5000 AND p.fecha_hora_salida BETWEEN "2019-09-21 00:00:00" AND "2019-09-21 11:59:00";


select r.id_ruta, count(b.id_boleto) as cant_boletos, sum(b.precio_total) as total from boleto b, pasaje p, ruta r where b.id_pasaje = p.id_pasaje and p.id_ruta = r.id_ruta and r.orden = 1 and b.fecha_hora_compra like "%2019-09%" GROUP by r.id_ruta;


