-- MariaDB dump 10.17  Distrib 10.4.6-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: buses
-- ------------------------------------------------------
-- Server version	10.4.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `boleto`
--

DROP TABLE IF EXISTS `boleto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boleto` (
  `id_boleto` varchar(10) NOT NULL,
  `id_pasaje` varchar(10) NOT NULL,
  `rut_usuario` int(9) NOT NULL,
  `num_asiento` int(2) NOT NULL,
  `fecha_hora_compra` datetime NOT NULL,
  `precio_total` int(5) NOT NULL,
  PRIMARY KEY (`id_boleto`),
  KEY `id_pasaje` (`id_pasaje`),
  KEY `rut_usuario` (`rut_usuario`),
  CONSTRAINT `boleto_ibfk_1` FOREIGN KEY (`id_pasaje`) REFERENCES `pasaje` (`id_pasaje`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `boleto_ibfk_2` FOREIGN KEY (`rut_usuario`) REFERENCES `usuario` (`rut_usuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boleto`
--

LOCK TABLES `boleto` WRITE;
/*!40000 ALTER TABLE `boleto` DISABLE KEYS */;
INSERT INTO `boleto` VALUES ('1','66666fffff',186431626,20,'2019-09-22 17:35:51',5000),('2','66666fffff',123456789,21,'2019-09-22 17:55:27',5000);
/*!40000 ALTER TABLE `boleto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bus`
--

DROP TABLE IF EXISTS `bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus` (
  `num_bus` int(6) NOT NULL AUTO_INCREMENT,
  `tipo_bus` int(1) NOT NULL,
  `patente` varchar(6) NOT NULL,
  PRIMARY KEY (`num_bus`),
  KEY `tipo_bus` (`tipo_bus`),
  CONSTRAINT `bus_ibfk_1` FOREIGN KEY (`tipo_bus`) REFERENCES `tipo_bus` (`tipo_bus`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus`
--

LOCK TABLES `bus` WRITE;
/*!40000 ALTER TABLE `bus` DISABLE KEYS */;
INSERT INTO `bus` VALUES (1,1,'ASDF12'),(2,1,'ASDF22'),(3,2,'ASDF33'),(4,2,'ASDF44');
/*!40000 ALTER TABLE `bus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comuna`
--

DROP TABLE IF EXISTS `comuna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comuna` (
  `id_comuna` int(3) NOT NULL AUTO_INCREMENT,
  `nombre_comuna` varchar(50) NOT NULL,
  PRIMARY KEY (`id_comuna`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comuna`
--

LOCK TABLES `comuna` WRITE;
/*!40000 ALTER TABLE `comuna` DISABLE KEYS */;
INSERT INTO `comuna` VALUES (1,'Iquique'),(2,'Antofagasta'),(3,'La Serena'),(4,'Valparaiso'),(5,'Santiago'),(6,'Temuco');
/*!40000 ALTER TABLE `comuna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `convenio`
--

DROP TABLE IF EXISTS `convenio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `convenio` (
  `id_convenio` int(2) NOT NULL AUTO_INCREMENT,
  `convenio` varchar(50) NOT NULL,
  `desc_convenio` float NOT NULL,
  PRIMARY KEY (`id_convenio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convenio`
--

LOCK TABLES `convenio` WRITE;
/*!40000 ALTER TABLE `convenio` DISABLE KEYS */;
/*!40000 ALTER TABLE `convenio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `convenio_usuario`
--

DROP TABLE IF EXISTS `convenio_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `convenio_usuario` (
  `rut_usuario` int(9) NOT NULL,
  `id_convenio` int(2) NOT NULL,
  PRIMARY KEY (`rut_usuario`,`id_convenio`),
  KEY `id_convenio` (`id_convenio`),
  CONSTRAINT `convenio_usuario_ibfk_1` FOREIGN KEY (`rut_usuario`) REFERENCES `usuario` (`rut_usuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `convenio_usuario_ibfk_2` FOREIGN KEY (`id_convenio`) REFERENCES `convenio` (`id_convenio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convenio_usuario`
--

LOCK TABLES `convenio_usuario` WRITE;
/*!40000 ALTER TABLE `convenio_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `convenio_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pasaje`
--

DROP TABLE IF EXISTS `pasaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pasaje` (
  `id_pasaje` varchar(10) NOT NULL,
  `num_bus` int(6) NOT NULL,
  `id_ruta` int(6) NOT NULL,
  `precio` int(5) NOT NULL,
  `fecha_hora_salida` datetime NOT NULL,
  `fecha_hora_llegada` datetime NOT NULL,
  PRIMARY KEY (`id_pasaje`),
  KEY `num_bus` (`num_bus`),
  KEY `id_ruta` (`id_ruta`),
  CONSTRAINT `pasaje_ibfk_1` FOREIGN KEY (`num_bus`) REFERENCES `bus` (`num_bus`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pasaje_ibfk_2` FOREIGN KEY (`id_ruta`) REFERENCES `ruta` (`id_ruta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pasaje`
--

LOCK TABLES `pasaje` WRITE;
/*!40000 ALTER TABLE `pasaje` DISABLE KEYS */;
INSERT INTO `pasaje` VALUES ('11111aaaaa',2,1,5000,'2019-09-21 10:00:00','2019-09-21 14:00:00'),('22222bbbbb',2,2,3000,'2019-09-21 16:00:00','2019-09-21 18:00:00'),('33333ccccc',1,5,7000,'2019-09-21 10:00:00','2019-09-21 18:00:00'),('44444ddddd',3,3,5000,'2019-09-21 12:00:00','2019-09-21 16:00:00'),('55555eeeee',4,2,5000,'2019-09-21 10:30:00','2019-09-21 16:30:00'),('66666fffff',1,7,5000,'2019-09-21 10:30:00','2019-09-21 16:30:00'),('77777ggggg',3,7,5000,'2019-09-21 10:35:00','2019-09-21 16:35:00');
/*!40000 ALTER TABLE `pasaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ruta`
--

DROP TABLE IF EXISTS `ruta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ruta` (
  `id_ruta` int(6) NOT NULL,
  `id_terminal` int(3) NOT NULL,
  `orden` int(1) NOT NULL,
  PRIMARY KEY (`id_ruta`,`id_terminal`),
  KEY `id_terminal` (`id_terminal`),
  CONSTRAINT `ruta_ibfk_1` FOREIGN KEY (`id_terminal`) REFERENCES `terminal` (`id_terminal`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ruta`
--

LOCK TABLES `ruta` WRITE;
/*!40000 ALTER TABLE `ruta` DISABLE KEYS */;
INSERT INTO `ruta` VALUES (1,1,1),(1,3,2),(1,4,3),(2,4,1),(2,5,3),(3,4,3),(3,6,1),(4,1,3),(4,2,2),(4,6,1),(5,2,3),(5,3,1),(6,4,3),(6,5,1),(7,4,1),(7,6,3);
/*!40000 ALTER TABLE `ruta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefono`
--

DROP TABLE IF EXISTS `telefono`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefono` (
  `rut_usuario` int(9) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  PRIMARY KEY (`rut_usuario`,`telefono`),
  CONSTRAINT `telefono_ibfk_1` FOREIGN KEY (`rut_usuario`) REFERENCES `usuario` (`rut_usuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefono`
--

LOCK TABLES `telefono` WRITE;
/*!40000 ALTER TABLE `telefono` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefono` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terminal`
--

DROP TABLE IF EXISTS `terminal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `terminal` (
  `id_terminal` int(3) NOT NULL AUTO_INCREMENT,
  `id_comuna` int(3) NOT NULL,
  `nombre_terminal` varchar(50) NOT NULL,
  PRIMARY KEY (`id_terminal`),
  KEY `id_comuna` (`id_comuna`),
  CONSTRAINT `terminal_ibfk_1` FOREIGN KEY (`id_comuna`) REFERENCES `comuna` (`id_comuna`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terminal`
--

LOCK TABLES `terminal` WRITE;
/*!40000 ALTER TABLE `terminal` DISABLE KEYS */;
INSERT INTO `terminal` VALUES (1,1,'Tnal. Esmeralda'),(2,2,'Rodov. Antofagasta'),(3,3,'Serena Of.E3 TB'),(4,4,'Rodov. Valparaiso'),(5,5,'T. Borja Andes.30-41'),(6,5,'Tnal. Alameda');
/*!40000 ALTER TABLE `terminal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_bus`
--

DROP TABLE IF EXISTS `tipo_bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_bus` (
  `tipo_bus` int(1) NOT NULL AUTO_INCREMENT,
  `cant_asientos` int(2) NOT NULL,
  `tipo_asientos` varchar(20) NOT NULL,
  `detalle_tipo` varchar(80) NOT NULL,
  PRIMARY KEY (`tipo_bus`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_bus`
--

LOCK TABLES `tipo_bus` WRITE;
/*!40000 ALTER TABLE `tipo_bus` DISABLE KEYS */;
INSERT INTO `tipo_bus` VALUES (1,32,'Clásico','Bus de un piso y un Baño'),(2,63,'Semi-Cama','Bus de dos pisos y un baño en el segundo piso');
/*!40000 ALTER TABLE `tipo_bus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `rut_usuario` int(9) NOT NULL,
  `rol_usuario` int(1) DEFAULT 0,
  `nombre` varchar(50) NOT NULL,
  `apellido_mat` varchar(50) NOT NULL,
  `apellido_pat` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rut_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (123456789,0,'Ignacio','Jerez','Tapia','ignacio.tapiaj@alumnos.uv.cl',NULL),(186431626,1,'Jeremy','Castro','Espinoza','jeremy.castroe@alumnos.uv.cl','asdasdasdasd');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-23 21:39:45
