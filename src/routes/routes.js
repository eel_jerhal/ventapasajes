const express = require('express');
const router = express.Router();

const comunasController = require('../controllers/comunasController');
const pasajesController = require('../controllers/pasajesController');
const boletosController = require('../controllers/boletosController');

/* COMUNAS */
router.get('/getorigenes', comunasController.getOrigenList);
router.get('/getdestinos', comunasController.getDestinoList);
router.get('/insertcomuna/:nombre', comunasController.insertComuna); /*Debería ser por metodo POST desde un FORM */

/* PASAJES */

router.get('/listapasajes', pasajesController.getPasajes);
router.get('/crearpasaje', pasajesController.insertPasajes);
router.get('/editarpasaje', pasajesController.updatePasajes);
router.get('/eliminarpasaje/:id', pasajesController.deletePasaje);

/* BOLETOS */
router.get('/listaboletos', boletosController.getBoletos);
router.get('/agregarboleto', boletosController.insertBoletos);

module.exports = router;