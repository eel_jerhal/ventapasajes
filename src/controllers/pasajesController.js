const controller = {};

controller.getPasajes = (req,res) => {
    /*DATOS VIENEN DESDE EL FILTRO DE LA VISTA */
    const data = {  // * Son obligatorios
        "origen": 4,    //*
        "destino": 5,   //*
        "horario_salida": null,
        "fecha_salida_1": "2019-09-21", //*
        "fecha_salida_2": "",
        "precio": 5000
    };

    if(data.horario_salida != null){ /* Juntar fecha con hora YYYY-MM-DD hh:mm:ss */
        data.fecha_salida_2 = data.fecha_salida_1;
        switch (data.horario_salida) {
            case 0:
                data.fecha_salida_1 += " 00:00:00";
                data.fecha_salida_2 += " 11:59:00"; 
                break;
            case 1:
                data.fecha_salida_1 += " 12:00:00";
                data.fecha_salida_2 += " 17:59:00"; 
                break;
            case 2:
                data.fecha_salida_1 += " 18:00:00";
                data.fecha_salida_2 += " 23:59:00"; 
                break;
            default:
                break;
        }
    }else{
        data.fecha_salida_2 = data.fecha_salida_1 + " 23:59:59";
    }

    req.getConnection((err, conn) =>{
        conn.query('SELECT p.id_pasaje, tb.cant_asientos, tb.tipo_asientos , p.precio, t1.c_origen, p.fecha_hora_salida, t1.t_origen, t1.c_destino, p.fecha_hora_llegada, t1.t_destino ' +
        'FROM pasaje p, bus b, tipo_bus tb, ' +
        '(SELECT r1.id_ruta, r1.id_comuna AS c_id_origen, r1.nombre_comuna AS c_origen, r1.id_terminal AS t_id_origen, r1.nombre_terminal AS t_origen, ' +
          'r2.id_comuna AS c_id_destino, r2.nombre_comuna AS c_destino, r2.id_terminal AS t_id_destino, r2.nombre_terminal AS t_destino, r2.orden  ' +
          'FROM (SELECT r.id_ruta, r.orden, t.id_terminal, t.nombre_terminal, c.id_comuna, c.nombre_comuna FROM ruta r, terminal t, comuna c ' +
          'WHERE r.id_terminal = t.id_terminal AND t.id_comuna = c.id_comuna) r1 LEFT OUTER JOIN ' +
          '(SELECT r.id_ruta, r.orden, t.id_terminal, t.nombre_terminal, c.id_comuna, c.nombre_comuna FROM ruta r, terminal t, comuna c WHERE r.id_terminal = t.id_terminal AND t.id_comuna = c.id_comuna) r2 ON r1.id_ruta = r2.id_ruta AND r1.id_terminal != r2.id_terminal ' +
          'WHERE r1.orden = 1 AND r2.orden != 1 ORDER BY r1.id_ruta) t1 ' +
        'WHERE p.num_bus = b.num_bus AND b.tipo_bus = tb.tipo_bus AND t1.id_ruta = p.id_ruta AND t1.c_id_origen = ? AND t1.c_id_destino = ? AND p.precio <= ? AND p.fecha_hora_salida BETWEEN ? AND ?;', [data.origen, data.destino, data.precio, data.fecha_salida_1, data.fecha_salida_2],(err, row) =>{
            if (err) {
                console.error(err.message);
            }else{
                res.render('listaPasajes', {
                    pasaje: row,
                    origen: data.origen,
                    destino: data.destino  
                });
            }
        });
    });
};

controller.insertPasajes = (req,res) => {
    /*DATOS VIENEN DESDE EL FORM DE LA VISTA */
    const p = {
        "id_pasaje": "3214asda",
        "num_bus": 5,
        "id_ruta": 3,
        "precio": 4500,
        "fecha_hora_salida": "2019-09-25 16:00:00",
        "fecha_hora_llegada": "2019-09-25 20:00:00",
        "mensaje": ""
    };

    req.getConnection((err, conn) =>{
        conn.query('INSERT INTO pasaje values(?,?,?,?,?,?);', [p.id_pasaje, p.num_bus, p.id_ruta, p.precio, p.fecha_hora_salida, p.fecha_hora_llegada],(err, row) =>{
            if (err) {
                console.log(err.message);   
            }else{
                p.mensaje = "Pasaje " + p.id_pasaje + " Ingresado con Éxito!";
                res.render('agregarPasajes', {
                    pasaje: p
                });
            }
        });
    });
};

controller.updatePasajes = (req,res) => {
    const p = {
        "id_pasaje": "88888hhhhh",
        "precio": 8000,
    };
    req.getConnection((err, conn) =>{
        conn.query('UPDATE pasaje SET precio = ? WHERE id_pasaje = ?;', [p.precio, p.id_pasaje],(err, row) =>{
            if (err) {
                res.render('mensajesPrueba', {
                    mensaje: err.message
                });   
            } else {
                res.render('mensajesPrueba', {
                    mensaje: "Pasaje actualizado con éxito."
                });
            }
        });
    });
};

controller.deletePasaje = (req,res) => {
    const {id}  = req.params;
    req.getConnection((err, conn) =>{
        conn.query('DELETE FROM pasaje WHERE id_pasaje = ?', id, (err, rows) =>{
            if (err) {
                console.log(err.message);
                res.render('mensajesPrueba', {
                    mensaje: "Error al eliminar."
                });
            }else{
                console.log(rows);
                res.render('mensajesPrueba', {
                    mensaje: "Pasaje eliminado con éxito."
                });
            }
        });
    });
};


module.exports = controller;