const controller = {};

controller.getBoletos = (req,res) => {
    /*DATOS VIENEN DESDE EL FILTRO DE LA VISTA */
    const data = {
        "usuario": 1,
        "fecha": "2019-09",
        "rut_usuario": "123456789"
    };
    //Si es admin
    if (data.usuario == 1) {
        req.getConnection((err, conn) =>{
            conn.query('select r.id_ruta, count(b.id_boleto) as cant_boletos, sum(b.precio_total) as total ' + 
            'from boleto b, pasaje p, ruta r ' + 
            'where b.id_pasaje = p.id_pasaje and p.id_ruta = r.id_ruta and r.orden = 1 and b.fecha_hora_compra like "%' + data.fecha +'%" GROUP by r.id_ruta;',(err, row) =>{
                if (err) {
                    console.error(err.message);
                    res.render('mensajesPrueba', {
                        mensaje: "Error, Ver Consola."
                    });
                }else{
                    console.log(row);
                    res.render('mensajesPrueba', {
                        mensaje: "Datos del admin cargados, Ver Consola."
                    });
                }
            });
        });
    }else{  //si es un comprador
        req.getConnection((err, conn) =>{
            conn.query('select id_boleto, id_pasaje, num_asiento, fecha_hora_compra, precio_total from boleto where rut_usuario = ?;', data.rut_usuario,(err, row) =>{
                if (err) {
                    console.error(err.message);
                    res.render('mensajesPrueba', {
                        mensaje: "Error, Ver Consola."
                    });
                }else{
                    console.log(row);
                    res.render('mensajesPrueba', {
                        mensaje: "Datos cargados, Ver Consola."
                    });
                }
            });
        });
    }
};

controller.insertBoletos = (req,res) => {
    const b = { //datos que vienen de la vista
        "id_boleto": "9",
        "id_pasaje": "33333ccccc",
        "rut_usuario": "111111111",
        "num_asiento": "4",
        "fecha_hora_compra": "2019-09-25 02:00:00",
        "precio_total": "7000",
        "mensaje": ""
    };

    req.getConnection((err, conn) =>{
        conn.query('INSERT INTO boleto values(?,?,?,?,?,?);', [b.id_boleto, b.id_pasaje, b.rut_usuario, b.num_asiento, b.fecha_hora_compra, b.precio_total],(err, row) =>{
            if (err) {
                console.log(err.message);   
            }else{
                b.mensaje = "Boleto " + b.id_boleto + " Ingresado con Éxito!";
                res.render('mensajesPrueba', {
                    mensaje: b.mensaje
                });
            }
        });
    });

};

module.exports = controller;