const controller = {};

controller.getOrigenList = (req,res) => {
    req.getConnection((err, conn) =>{
        conn.query('SELECT DISTINCT c.nombre_comuna FROM ruta r, terminal t, comuna c WHERE r.orden = 1 AND r.id_terminal = t.id_terminal and t.id_comuna = c.id_comuna GROUP BY r.id_terminal ORDER BY c.nombre_comuna;',(err, row) =>{
            if (err) {
                console.log(err);   
            }else{
                console.log(row);
            }
        });
    });
};

controller.getDestinoList = (req,res) => {
    req.getConnection((err, conn) =>{
        conn.query('SELECT DISTINCT c.id_comuna, c.nombre_comuna FROM ruta r, terminal t, comuna c WHERE r.orden != 1 AND r.id_terminal = t.id_terminal and t.id_comuna = c.id_comuna GROUP BY r.id_terminal ORDER BY c.nombre_comuna;',(err, row) =>{
            if (err) {
                console.log(err);   
            }else{
                console.log(row);
            }
        });
    });
};

controller.insertComuna = (req,res) => {
    const {nombre}  = req.params;
    req.getConnection((err, conn) =>{
        conn.query('INSERT INTO comuna (nombre_comuna) values(?);', nombre,(err, row) =>{
            if (err) {
                console.log(err);   
            }else{
                console.log(row);
            }
        });
    });
};

module.exports = controller;